# SPDX-FileCopyrightText: 2022 Endo Renberg
#
# SPDX-License-Identifier: ISC

## .. include:: ../canons.rst

import std/[asyncdispatch, streams, sysrand]
import cbor, eris

export cbor

proc writeCborHook(str: Stream; cap: ErisCap) =
  writeCborTag(str, erisCborTag)
  var bin = cap.bytes
  writeCbor(str, bin)

proc fromCborHook(v: var ErisCap; n: CborNode): bool =
  if n.hasTag(erisCborTag) and n.kind == cborBytes and n.bytes.len == 66:
    try:
      v = parseCap(n.bytes)
      result = true
    except ValueError: discard

const padItem = "\x80"

type
  Canon = object of RootObj
    buffer: StringStream
  ReadCanon* = ref object of Canon
    ## Object representing a Cannon that can iterated.
    blocks: ErisStream
    parser: CborParser
  WriteCanon* = ref object of Canon
    ## Object representing a Cannon that can be appended to.
    ingest: ErisIngest

proc newCanon*(store: ErisStore; blockSize: BlockSize; secret: Secret): WriteCanon =
  ## Create a new `WriteCanon`.
  ## The `Secret` should be sufficiently random to provide cryptographic confidentiality.
  WriteCanon(
    buffer: newStringStream(),
    ingest: newErisIngest(store, blockSize, secret))

proc newCanon*(store: ErisStore; blockSize: BlockSize): WriteCanon =
  ## Create a new `WriteCanon`.
  ## This procedure uses a random number generator provided by the
  ## operating system to generate a unique convergence secret.
  var secret: Secret
  doAssert urandom(secret.bytes)
  newCanon(store, blockSize, secret)

proc reopenCanon*(store: ErisStore; cap: ErisCap; secret: Secret): Future[WriteCanon] {.async.} =
  ## Re-open a Canon for appending.
  let ingest = await reopenErisIngest(store, cap, secret)
  return WriteCanon(
    buffer: newStringStream(),
    ingest: ingest)

proc reopenCanon*(store: ErisStore; cap: ErisCap): Future[WriteCanon] =
  ## Re-open a Canon for appending.
  ## This procedure uses a random number generator provided by the
  ## operating system to generate a unique convergence secret.
  var secret: Secret
  doAssert urandom(secret.bytes)
  reopenCanon(store, cap, secret)

proc openCanon*(store: ErisStore; cap: ErisCap): ReadCanon =
  ## Open a `ReadCanon` for a given `ErisStore` and `ErisCap`.
  ReadCanon(
    buffer: newStringStream(),
    blocks: newErisStream(store, cap))

proc cap*(rc: ReadCanon): ErisCap = rc.blocks.cap
  ## Get the `ErisCap` of a `ReadCanon`.

proc cap*(wc: WriteCanon): Future[ErisCap] = wc.ingest.cap()
  ## Get the current `ErisCap` of the `WriteCanon`.
  ## This procedure commits some blocks to the backend `ErisStore`
  ## as a side-effect but the canon is still appendable after calling this.

proc append*[T](wc: WriteCanon; item: T): Future[BiggestUInt] {.async.} =
  ## Append `item` to a `WriteCanon`.
  ## The type `T` must be valid for `writeCbor(s: Stream; item: T)`.
  wc.buffer.setPosition 0
  wc.buffer.writeCbor(item)
  wc.buffer.data.setLen(wc.buffer.getPosition)
  assert wc.buffer.data != padItem, "cannot append an empty array"
  let
    itemLen = wc.buffer.data.len
    blockSize = wc.ingest.blockSize.int
  result = wc.ingest.position
  if itemLen < blockSize:
    if blockSize < ((blockSize.pred and wc.ingest.position.int) + itemLen):
      await wc.ingest.padToNextBlock()
    await wc.ingest.append(wc.buffer.data)
  else:
    let blockSize = if itemLen < 16 shl 10: bs1k else: bs32k
    var cap = await encode(wc.ingest.store, blockSize, wc.buffer, wc.ingest.secret)
    result = await wc.append(cap)

proc getPosition*(rc: ReadCanon): BiggestUInt =
  ## Get the current position of a `ReadCanon` as a
  ## byte offset from the first byte of the canon.
  let
    blockSize = rc.blocks.cap.blockSize.BiggestUInt
    blockPos = rc.blocks.getPosition and (not blockSize.pred)
  if blockPos > 1:
    blockPos - 1 + rc.buffer.getPosition.BiggestUInt
  else:
    rc.buffer.getPosition.BiggestUInt

proc setPosition*(rc: ReadCanon; pos: BiggestUInt) {.async.} =
  ## Set the position of a `ReadCanon` as a
  ## byte offset from the first byte of the canon.
  let
    blockMask = not rc.blocks.cap.blockSize.BiggestUInt.pred
    curBlockPos = rc.getPosition and blockMask
  if pos < curBlockPos or rc.blocks.cap.blockSize.BiggestUInt < (pos - curBlockPos):
    rc.buffer.data.setLen(rc.blocks.cap.blockSize.int)
    let blockPos = pos and blockMask
    rc.blocks.setPosition(blockPos)
    let n = await rc.blocks.readDataStr(rc.buffer.data, 0..rc.buffer.data.high)
    rc.buffer.data.setLen(n)
  rc.buffer.setPosition(0)
  open(rc.parser, rc.buffer)
  rc.parser.next()
  let blockOffset = int pos and (not blockMask)
  while rc.parser.position < blockOffset:
    rc.parser.skipNode()
  if rc.parser.position != blockOffset:
    raise newException(IoError, "setPosition did not align with a CBOR item")

proc map*[T](rc: ReadCanon; op: proc (index: BiggestUInt; x: T): bool {.closure.}) {.async.} =
  ## Call `op` for every successive item that is convertable to `T`
  ## in `ReadCanon` until `op` returns false.
  var
    progress = true
    blockPosition: BiggestUInt
  while progress:
    if rc.buffer.getPosition == rc.buffer.data.len:
      let blockSize = rc.blocks.cap.blockSize.int
      if rc.buffer.data.len == 0:
        rc.buffer.data.setLen(blockSize)
      elif rc.buffer.data.len < blockSize:
        break
      rc.buffer.setPosition 0
      let n = await rc.blocks.readDataStr(rc.buffer.data, 0..rc.buffer.data.high)
      if n != blockSize:
        rc.buffer.data.setLen(n)
      blockPosition = rc.blocks.getPosition - n.BiggestUInt
      open(rc.parser, rc.buffer)
      rc.parser.next()
    while rc.parser.kind == CborEventKind.cborArray and rc.parser.arrayLen == 0:
      rc.parser.next()
    if rc.parser.kind != CborEventKind.cborEof:
      var
        index = blockPosition + rc.parser.position.BiggestUInt
        node = rc.parser.nextNode()
      if node.hasTag(erisCborTag):
        var cap: ErisCap
        if not cap.fromCbor(node):
          raise newException(ValueError, "Canon contains invalid ERIS capability")
        var tmp = await newErisStream(rc.blocks.store, cap).readAll
        node = parseCbor(cast[string](tmp))
      when T is CborNode:
        progress = op(index, node)
      else:
        var x: T
        if x.fromCbor(node):
          progress = op(index, x)
