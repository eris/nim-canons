Canons are an append-only-log construction of CBOR data optimized for storage and retrieval using ERIS encoding.

Canons expose a traversable and appendable sequence of CBOR items to applications. Internally the Canon is a sequence of binary encoded CBOR items that are aligned to fit within ERIS block boundaries (1KiB or 32KiB) using zero-length CBOR arrays as padding. This is an optimization to minimize the number of ERIS meta-blocks created over the lifetime of the canon and allows for efficient lookup or selective sharing of Canon items. An item within a canon can be referenced by an ERIS URNs combined with an offset index.

Items that are too large to fit within a single block are replaced by an `ERIS read-capability <https://eris.codeberg.page/spec/#_binary_encoding_of_read_capability>`_ to an off-canon encoding.

The recommended ERIS encoding of a Canon is unique rather than `convergent <https://eris.codeberg.page/spec/#_convergence_secret>`_ to mitigate confirmation attacks against Canon data at-rest.

Some of the invariant properties of Canons:

- All items within the raw Canon are less than the block size minus one. Off-canon items may be of arbitrary length.

- Each block of the Canon is terminated by one or more zero-length arrays. If a single block or the final block of a Canon is retrieved from ERIS storage then one or more of the terminating zero-length arrays will be missing. This is because the normalized encoding of a zero-length array is a single byte with a value of 0x80, and ERIS padding is defined as the byte 0x80 followed by zero or more null bytes.

- Zero-length arrays may be appended to a Canon but are discarded during traversal.

- ERIS read-capabilities in `CBOR encoding <https://eris.codeberg.page/spec/#_binary_encoding_of_read_capability>`_ may be appended but are dereferenced during traversal. Appending a capability that does not deference to CBOR data will invalidate a Canon.
- The former two rules do not apply to items nested within CBOR arrays, maps, or tags.
