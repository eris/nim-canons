# SPDX-FileCopyrightText: 2022 Endo Renberg
#
# SPDX-License-Identifier: ISC

import std/[asyncdispatch, random, times, unittest]

import eris, eris/stores, canons

type Message = tuple
  date: DateTime
  text: string

const
  textA = "All work and no play makes Jack a dull boy"
  textB = "All play and no work makes Jack a mere toy"

test "canons":
  const iterations = 1 shl 11
  let initialDate = parse(CompileDate, "yyyy-MM-dd")
  var
    store = newMemoryStore()
    cap: ErisCap
    index: BiggestUInt
  block:
    var
      canon = newCanon(store, bs1k)
      rng = initRand(66)
      date = initialDate
    for i in 1..iterations:
      date += rng.rand(60).seconds
      index = waitFor canon.append (date, textA,)
    cap = waitFor canon.cap
  block:
    var
      canon = openCanon(store, cap)
      rng = initRand(66)
      date = initialDate
      msg: Message
      index: BiggestUint
      counter: int
      positions: seq[BiggestUint]
    block:
      # check all items are reachable from map
      waitFor canon.map do (index: BiggestUInt; msg: Message) -> bool:
        date += rng.rand(60).seconds
        check msg.date == date
        check msg.text == textA
        inc counter
        positions.add(index)
        true
      check counter == iterations
    block:
      # Check all indexes are valid
      shuffle(positions)
      for pos in positions:
        waitFor canon.setPosition(pos)
        var mapped = false
        waitFor canon.map do (index: BiggestUInt; msg: Message) -> bool:
          mapped = true
        check mapped
  block:
    # Check appending to reopened canons
    block:
      var writeable = waitFor reopenCanon(store, cap)
      discard waitFor writeable.append (initialDate, textB,)
      cap = waitFor writeable.cap
    block:
      var readable = openCanon(store, cap)
      waitFor readable.setPosition(index)
      waitFor readable.map do (index: BiggestUInt; msg: Message) -> bool:
        check msg.text == textA
      waitFor readable.map do (index: BiggestUInt; msg: Message) -> bool:
        check msg.text == textB
