# Canons of ERIS

Append-Only-Logs of [CBOR](http://cbor.io/) data encoded to [ERIS](https://eris.codeberg.page/).

See [canons.rst](./canons.rst) for more information.