# Package

version       = "0.1.0"
author        = "Endo Renberg"
description   = "ERIS immutable logs"
license       = "ISC"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6.4", "eris >= 0.8.0", "cbor >= 0.9.0"
